'use strict';

// ready
$(document).ready(function() {

    // mask type = phone
     $("[name=phone]").mask("+7 (999) 999-9999");
     $("[type=date]").mask("99.99.9999");

    // slider
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText:["<img src='images/owl-arrr.png'>","<img src='images/owl-arrl.png'>"],
        items:1
    });
    var $sync1 = $(".big-images"),
        $sync2 = $(".thumbs"),
        flag = false,
        duration = 300;
    $sync1.owlCarousel({
        items: 1,
        margin: 10,
        nav: false,
        dots: false
    })
    .on('changed.owl.carousel', function (e) {
        if (!flag) {
            flag = true;
            $sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
            flag = false;
        }
    });
    $sync2.owlCarousel({
        margin: 25,
        items: 2,
        nav: false,
        center: true,
        dots: false,
        mouseDrag: false,
        responsive : {
            390 : {
                items: 3
            },
            993 : {
                items: 4
            },
            1300 : {
                items: 6
            }
        }
    })
    .on('click', '.owl-item', function () {
        $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
    })
    .on('changed.owl.carousel', function (e) {
        if (!flag) {
            flag = true;
            $sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
            flag = false;
        }
    });

    // custom select
    if ($("select").length) {
        $('select').selectOrDie({
            onChange: function () {
                var el = $(this).val();
                $('.total .form-group').css('display','none');
                $('#div'+el).css('display','block');

                $("select").selectOrDie("update");
            }
        });
    }


    $('.review__item-link').click(function () {
        $(this).parent().parent().toggleClass('active');
        return false;
    });

    $(function () {
        $('[data-toggle="popover"]').popover()
    })

    //busket delete function
    $('.busket__delete-js a').click(function(){
        $(this).parent().parent().fadeOut('slow');
        return false;
    });

    // count input
    $('.product_quantity_js .inc').click(function () {
        var $input = $(this).parents('.product_quantity_js').find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.product_quantity_js .dec').click(function () {
        var $input = $(this).parents('.product_quantity_js').find('input');
        var count = parseInt($input.val()) + 1;
        count = count > 999 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });


    // range slider
    if ($("#rangeSlider").length) {
        var range = document.getElementById('rangeSlider');
        var start = document.getElementById('start');
        var end = document.getElementById('end');
        noUiSlider.create(range, {
            start: [300, 810],
            connect: true,
            step: 10,
            range: {
                'min': 0,
                'max': 1000
            }
        });
        range.noUiSlider.on('update', function (values, handle) {
            var value = values[handle];
            if (handle) {
                end.value = Math.round(value);
            } else {
                start.value = Math.round(value);
            }
        });
        end.addEventListener('change', function () {
            range.noUiSlider.set([this.value, null]);
        });
        start.addEventListener('change', function () {
            range.noUiSlider.set([null, this.value]);
        });
    }
    // range slider


    // .collapse
    $(".collapse__elem-title a").click(function () {
        $(this).parent().hide().next().show('slow');
        return false;
    });
    // .collapse


    $(".mainmenu--js").click(function () {
        $(this).parents().find('.mainmenublock--js').addClass('active').parents().find('.slide-overlay').addClass('active');
        return false;
    });
    $(".slide-overlay, .close--js").click(function () {
        $(this).parents().find('.mainmenublock--js').removeClass('active').parents().find('.slide-overlay').removeClass('active');
        return false;
    });

    // mobile filter
    function blockTog(className){
        var className;
        $(className + '__title-js').click(function(){
            $(this).toggleClass('active').parents().find(className).slideToggle();
            return false;
        });
    }
    blockTog(".filter");
    blockTog(".sort");
    blockTog(".map");
    // mobile filter


    //$('.colorType-js a').click(function () {
    //    $('.colorType-js a').removeClass('active');
    //    $(this).addClass('active');
    //    return false;
    //});
    $(function() {
        $('.colorType-js').on('click', 'li', function() {
            $(this).addClass('active').siblings().removeClass('active');
            //return false;
        });
        $('.packageType').on('click', 'label', function() {
            $(this).parent().parent().addClass('active').siblings().removeClass('active');
            //return false;
        });
    });

    $(function(){
        $('.radio-block_inner input').click(function() {
            $('.radio-block').each(function(indx, element){
                if ($('input',this)[0].checked) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }  });
            if($('.tab1').hasClass('active')) {
                $('#tab1').show();
            } else {
                $('#tab1').hide();
            }
            if($('.tab2').hasClass('active')) {
                $('#tab2').show();
            } else {
                $('#tab2').hide();
            }
            if($('.tab3').hasClass('active')) {
                $('#tab3').show();
            } else {
                $('#tab3').hide();
            }
        }).filter(":checked").click();
    });


    // input type ONLY number
    jQuery.fn.ForceNumericOnly =
        function()
        {
            return this.each(function()
            {
                $(this).keydown(function(e)
                {
                    var key = e.charCode || e.keyCode || 0;
                    // Разрешаем backspace, tab, delete, стрелки, обычные цифры и цифры на дополнительной клавиатуре
                    return (
                    key == 8 ||
                    key == 9 ||
                    key == 46 ||
                    key == 190 ||
                    (key >= 37 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
                });
            });
        };
    $(".product_quantity_js input").ForceNumericOnly();


});
// ready

// load
//$(document).load(function() {});
// load

// scroll
//$(window).scroll(function() { });
// scroll

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 768) {}
// mobile sctipts